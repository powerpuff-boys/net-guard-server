-- --------------------------------------------------------
-- Host:                         asig-sql1-dev
-- Server version:               5.5.31-1~dotdeb.0-log - (Debian)
-- Server OS:                    debian-linux-gnu
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for netguard
CREATE DATABASE IF NOT EXISTS `netguard` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `netguard`;


-- Dumping structure for table netguard.bandwidths
CREATE TABLE IF NOT EXISTS `bandwidths` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `value` int(11) NOT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '0 - inactive, 1 - active',
  `created` datetime NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='Possible bandwidth values';

-- Dumping data for table netguard.bandwidths: ~6 rows (approximately)
/*!40000 ALTER TABLE `bandwidths` DISABLE KEYS */;
INSERT INTO `bandwidths` (`id`, `value`, `status`, `created`, `updated`) VALUES
	(1, 2048, 1, '2016-08-30 14:54:55', '2016-08-30 15:07:38'),
	(2, 5120, 1, '2016-08-30 14:55:41', '2016-08-30 15:07:31'),
	(3, 10240, 1, '2016-08-30 15:04:55', '2016-08-30 15:07:21'),
	(4, 20480, 1, '2016-08-30 15:05:04', '2016-08-30 15:07:17'),
	(5, 51200, 1, '2016-08-30 15:05:12', '2016-08-30 15:07:07'),
	(6, 102400, 1, '2016-08-30 15:06:59', '2016-08-30 15:07:00');
/*!40000 ALTER TABLE `bandwidths` ENABLE KEYS */;


-- Dumping structure for table netguard.devices
CREATE TABLE IF NOT EXISTS `devices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT 'Device name',
  `owner` varchar(255) NOT NULL COMMENT 'Name of owner',
  `address` varchar(255) DEFAULT NULL COMMENT 'Device address - description',
  `bandwidth_id` tinyint(4) DEFAULT NULL COMMENT 'Allocated bandwidth',
  `device_type_id` tinyint(4) DEFAULT NULL COMMENT 'Device type',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0 - inactive, 1 - active',
  `created` datetime NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `bandwidth_id` (`bandwidth_id`),
  KEY `device_type_id` (`device_type_id`),
  CONSTRAINT `FK_devices_bandwidths` FOREIGN KEY (`bandwidth_id`) REFERENCES `bandwidths` (`id`),
  CONSTRAINT `FK_devices_device_types` FOREIGN KEY (`device_type_id`) REFERENCES `device_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='All our devices';

-- Dumping data for table netguard.devices: ~2 rows (approximately)
/*!40000 ALTER TABLE `devices` DISABLE KEYS */;
INSERT INTO `devices` (`id`, `name`, `owner`, `address`, `bandwidth_id`, `device_type_id`, `status`, `created`, `updated`) VALUES
	(1, 'Laptop Dell', 'Traian Bratucu', 'Swan, zona MKTP', 6, 3, 1, '2016-08-30 15:27:07', '2016-09-04 00:54:48'),
	(4, 'Smartphone SGS4', 'Alexandru Puscu', 'Swan office park', 3, 5, 1, '2016-09-03 14:14:18', '2016-09-03 15:53:00'),
	(5, 'John\'s tablet', 'John G', 'Birou Swan et 2', 3, 2, 1, '2016-09-04 00:12:04', '2016-09-04 00:12:04');
/*!40000 ALTER TABLE `devices` ENABLE KEYS */;


-- Dumping structure for table netguard.device_types
CREATE TABLE IF NOT EXISTS `device_types` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '0 - inactive, 1 - active',
  `created` datetime NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='Possible device types';

-- Dumping data for table netguard.device_types: ~6 rows (approximately)
/*!40000 ALTER TABLE `device_types` DISABLE KEYS */;
INSERT INTO `device_types` (`id`, `name`, `status`, `created`, `updated`) VALUES
	(1, 'Smartphone', 1, '2016-08-30 14:57:30', '2016-08-30 14:57:31'),
	(2, 'Tablet', 1, '2016-08-30 14:57:41', '2016-08-30 14:57:41'),
	(3, 'Laptop / PC', 1, '2016-08-30 14:57:50', '2016-08-30 14:57:50'),
	(4, 'Smart TV', 1, '2016-08-30 14:57:58', '2016-08-30 14:57:59'),
	(5, 'Smart Appliance', 1, '2016-08-30 14:58:08', '2016-08-30 14:58:09'),
	(6, 'Other', 1, '2016-08-30 14:58:15', '2016-08-30 14:58:15');
/*!40000 ALTER TABLE `device_types` ENABLE KEYS */;


-- Dumping structure for table netguard.fos_user_group
CREATE TABLE IF NOT EXISTS `fos_user_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_583D1F3E5E237E06` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table netguard.fos_user_group: ~0 rows (approximately)
/*!40000 ALTER TABLE `fos_user_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `fos_user_group` ENABLE KEYS */;


-- Dumping structure for table netguard.fos_user_user
CREATE TABLE IF NOT EXISTS `fos_user_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `date_of_birth` datetime DEFAULT NULL,
  `firstname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `biography` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locale` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `twitter_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `gplus_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gplus_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gplus_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `two_step_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_C560D76192FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_C560D761A0D96FBF` (`email_canonical`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table netguard.fos_user_user: ~2 rows (approximately)
/*!40000 ALTER TABLE `fos_user_user` DISABLE KEYS */;
INSERT INTO `fos_user_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`, `created_at`, `updated_at`, `date_of_birth`, `firstname`, `lastname`, `website`, `biography`, `gender`, `locale`, `timezone`, `phone`, `facebook_uid`, `facebook_name`, `facebook_data`, `twitter_uid`, `twitter_name`, `twitter_data`, `gplus_uid`, `gplus_name`, `gplus_data`, `token`, `two_step_code`) VALUES
	(1, 'alexandru.puscu', 'alexandru.puscu', 'alexandru.puscu@emag.ro', 'alexandru.puscu@emag.ro', 1, 'ef09cw76wxs0cwogkkk08wgkwwg8c84', '20mVVas+PVXUdp4S4kQEGATS5dBlxYprcQ9vE8N2VMGJoum1FV/kVRUJ1eLZFrXyWUBqaT1Fp2EafPxfuTfTgw==', '2016-09-03 19:54:16', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:16:"ROLE_SUPER_ADMIN";}', 0, NULL, '2016-08-31 12:10:34', '2016-09-03 19:54:16', NULL, NULL, NULL, NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
	(2, 'admin', 'admin', 'admin@emag.ro', 'admin@emag.ro', 1, '236z2wg06o5c4ks0kocc84k8084k4cw', 'loyXIh5Xx18h/GR18NQxDXJBaaDVs5vyVCioXiZulpA7fE3ASTlDho6nb1csf2FIxXwksXlUYAhx6oYk+AIcFA==', '2016-09-04 00:10:01', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:16:"ROLE_SUPER_ADMIN";}', 0, NULL, '2016-08-31 17:12:12', '2016-09-04 00:10:01', NULL, NULL, NULL, NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL);
/*!40000 ALTER TABLE `fos_user_user` ENABLE KEYS */;


-- Dumping structure for table netguard.fos_user_user_group
CREATE TABLE IF NOT EXISTS `fos_user_user_group` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`group_id`),
  KEY `IDX_B3C77447A76ED395` (`user_id`),
  KEY `IDX_B3C77447FE54D947` (`group_id`),
  CONSTRAINT `FK_B3C77447FE54D947` FOREIGN KEY (`group_id`) REFERENCES `fos_user_group` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_B3C77447A76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user_user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table netguard.fos_user_user_group: ~0 rows (approximately)
/*!40000 ALTER TABLE `fos_user_user_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `fos_user_user_group` ENABLE KEYS */;


-- Dumping structure for table netguard.ip_addresses
CREATE TABLE IF NOT EXISTS `ip_addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) DEFAULT NULL,
  `ipv4` int(11) unsigned NOT NULL,
  `pop_id` tinyint(4) DEFAULT NULL,
  `mac` varchar(50) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '0 - inactive, 1 - active',
  `created` datetime NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `device_id` (`device_id`),
  KEY `status` (`status`),
  KEY `FK_ip_addresses_pops` (`pop_id`),
  CONSTRAINT `FK_ip_addresses_pops` FOREIGN KEY (`pop_id`) REFERENCES `pops` (`id`),
  CONSTRAINT `FK_ip_addresses_devices` FOREIGN KEY (`device_id`) REFERENCES `devices` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COMMENT='Allocated IP addresses, long format';

-- Dumping data for table netguard.ip_addresses: ~31 rows (approximately)
/*!40000 ALTER TABLE `ip_addresses` DISABLE KEYS */;
INSERT INTO `ip_addresses` (`id`, `device_id`, `ipv4`, `pop_id`, `mac`, `status`, `created`, `updated`) VALUES
	(2, 4, 3232235522, 1, NULL, 1, '2016-08-30 15:23:08', '2016-09-03 21:49:34'),
	(5, 5, 3232235525, 2, NULL, 1, '2016-08-30 15:23:38', '2016-09-04 00:12:04'),
	(6, 1, 3232235526, 2, NULL, 1, '2016-08-30 15:23:38', '2016-09-04 00:49:02'),
	(7, NULL, 3232235527, 2, NULL, 1, '2016-08-30 15:23:38', '2016-09-03 21:49:37'),
	(10, NULL, 3232235530, 1, NULL, 1, '2016-08-30 15:23:38', '2016-09-03 21:49:40'),
	(11, NULL, 3232235531, 1, NULL, 1, '2016-08-30 15:23:38', '2016-09-03 21:49:39'),
	(12, NULL, 3232235532, 1, NULL, 1, '2016-08-30 15:23:38', '2016-09-03 21:49:42'),
	(13, NULL, 3232235533, 1, NULL, 1, '2016-08-30 15:23:38', '2016-09-03 21:49:41'),
	(14, NULL, 3232235534, 2, NULL, 1, '2016-08-30 15:23:38', '2016-09-03 21:49:42'),
	(16, NULL, 3232235536, 2, NULL, 1, '2016-08-30 15:23:39', '2016-09-03 21:49:43'),
	(17, NULL, 3232235537, 2, NULL, 1, '2016-08-30 15:23:39', '2016-09-03 21:49:45'),
	(18, NULL, 3232235538, 2, NULL, 1, '2016-08-30 15:23:39', '2016-09-03 21:49:45'),
	(19, NULL, 3232235539, 2, NULL, 1, '2016-08-30 15:23:39', '2016-09-03 21:49:46'),
	(20, NULL, 3232235540, 2, NULL, 1, '2016-08-30 15:23:39', '2016-09-03 21:49:47'),
	(21, NULL, 3232235778, 1, NULL, 1, '2016-08-30 15:24:23', '2016-09-03 21:49:47'),
	(22, NULL, 3232235779, 1, NULL, 1, '2016-08-30 15:24:23', '2016-09-03 21:49:48'),
	(23, NULL, 3232235780, 1, NULL, 1, '2016-08-30 15:24:23', '2016-09-03 21:49:48'),
	(24, NULL, 3232235781, 1, NULL, 1, '2016-08-30 15:24:23', '2016-09-03 21:49:49'),
	(25, NULL, 3232235782, 1, NULL, 1, '2016-08-30 15:24:23', '2016-09-03 21:49:49'),
	(27, NULL, 3232235784, 1, NULL, 1, '2016-08-30 15:24:23', '2016-09-03 21:49:50'),
	(28, NULL, 3232235785, 1, NULL, 1, '2016-08-30 15:24:23', '2016-09-03 21:49:51'),
	(30, NULL, 3232235787, 1, NULL, 1, '2016-08-30 15:24:23', '2016-09-03 21:49:51'),
	(31, NULL, 3232235788, 1, NULL, 1, '2016-08-30 15:24:23', '2016-09-03 21:49:52'),
	(32, NULL, 3232235789, 1, NULL, 1, '2016-08-30 15:24:23', '2016-09-03 21:49:53'),
	(33, NULL, 3232235790, 1, NULL, 1, '2016-08-30 15:24:23', '2016-09-03 21:49:54'),
	(37, NULL, 3232235794, 1, NULL, 1, '2016-08-30 15:24:23', '2016-09-03 21:49:55'),
	(39, NULL, 3232235796, 1, NULL, 1, '2016-08-30 15:24:23', '2016-09-03 21:49:55');
/*!40000 ALTER TABLE `ip_addresses` ENABLE KEYS */;


-- Dumping structure for table netguard.pops
CREATE TABLE IF NOT EXISTS `pops` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '0 - inactive, 1 - active',
  `created` datetime NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Locations where we have our client scripts running';

-- Dumping data for table netguard.pops: ~2 rows (approximately)
/*!40000 ALTER TABLE `pops` DISABLE KEYS */;
INSERT INTO `pops` (`id`, `name`, `status`, `created`, `updated`) VALUES
	(1, 'Craiova', 1, '2016-08-30 14:55:57', '2016-08-30 14:55:58'),
	(2, 'Bucuresti', 1, '2016-08-30 14:56:11', '2016-08-30 14:56:12');
/*!40000 ALTER TABLE `pops` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
