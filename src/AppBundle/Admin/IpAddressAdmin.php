<?php

namespace AppBundle\Admin;

use Doctrine\Common\Collections\ArrayCollection;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class IpAddressAdmin
 *
 * @package AppBundle\Entity\Admin
 */
class IpAddressAdmin extends Admin
{
    public $last_position = 50;

    /** @var string */
    protected $baseRoutePattern = 'ip-address';

    /** @var string */
    protected $baseRouteName = 'ip-address';

    protected $datagridValues = [
        '_page'       => 1,
        '_sort_order' => 'DESC',
        '_sort_by'    => 'created',
    ];

    /**
     * @param ShowMapper $showMapper
     */
    public function configureShowFields(ShowMapper $showMapper)
    {
        parent::configureShowFields($showMapper);
        $showMapper->with('General')
            ->add('id')
            ->add('ipv4')
            ->add('mac')
            ->add('device')
            ->add('pointOfPresence', ArrayCollection::class)
            ->end();
        $showMapper->with('Statistics')
            ->add('status', 'boolean', ['label' => 'Active?'])
            ->add('created', 'datetime', ['label' => 'Creation date'])
            ->add('modified', 'datetime', ['label' => 'Last update date'])
            ->end();
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        parent::configureFormFields($formMapper);
        $formMapper
            ->with('Ip Address', ['class' => 'col-sm-4 col-md-4'])
            ->add('ipv4')
            ->add('mac', null, ['required' => false])
            ->add('pointOfPresence')
            ->add('status', ChoiceType::class, ['choices' => [1 => 'Active', 0 => 'Inactive']]);
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        parent::configureDatagridFilters($datagridMapper);
        $datagridMapper
            ->add('mac')
            ->add('pointOfPresence')
            ->add('device');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        parent::configureListFields($listMapper);
        $actions = ['show' => [], 'edit' => []];

        if (!$this->getSubject()) {
            $actions['delete'] = [];
        }
        $listMapper
            ->addIdentifier('id')
            ->add('ipv4')
            ->add('mac')
            ->add('device')
            ->add('pointOfPresence', ArrayCollection::class)
            ->add('status', 'boolean', ['editable' => true, 'label' => 'Active?'])
            ->add(
                '_action',
                'actions',
                [
                    'actions' => $actions
                ]
            );
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        // to remove a single route
    }
}
