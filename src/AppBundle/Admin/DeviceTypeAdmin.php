<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class DeviceTypeAdmin
 *
 * @package AppBundle\Entity\Admin
 */
class DeviceTypeAdmin extends Admin
{
    public $last_position = 50;

    /** @var string */
    protected $baseRoutePattern = 'device-type';

    /** @var string */
    protected $baseRouteName = 'device-type';

    protected $datagridValues = [
        '_page'       => 1,
        '_sort_order' => 'DESC',
        '_sort_by'    => 'created',
    ];

    /**
     * @param ShowMapper $showMapper
     */
    public function configureShowFields(ShowMapper $showMapper)
    {
        parent::configureShowFields($showMapper);
        $showMapper->with('General')
            ->add('id')
            ->add('name')
            ->end();
        $showMapper->with('Statistics')
            ->add('status', 'boolean', ['label' => 'Active?'])
            ->add('created', 'datetime', ['label' => 'Creation date'])
            ->add('modified', 'datetime', ['label' => 'Last update date'])
            ->end();
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        parent::configureFormFields($formMapper);
        $formMapper->with('General', ['class' => 'col-sm-3 col-md-3'])
            ->add('name')
            ->add('status', ChoiceType::class, ['choices' => [1 => 'Active', 0 => 'Inactive']])
            ->end();
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        parent::configureDatagridFilters($datagridMapper);
        $datagridMapper
            ->add('name');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        parent::configureListFields($listMapper);
        $listMapper
            ->add('id')
            ->add('name', null, ['editable' => true])
            ->add('status', 'boolean', ['editable' => true, 'label' => 'Active?'])
            ->add(
                '_action',
                'actions',
                [
                    'actions' => [
                        'show'   => [],
                        'edit'   => [],
                        'delete' => [],
                    ]
                ]
            );
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        // to remove a single route
//        $collection->remove('create');
    }
}
