<?php

namespace AppBundle\Admin;

use AppBundle\Entity\Device;
use AppBundle\Entity\IpAddress;
use Doctrine\Common\Collections\ArrayCollection;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class DeviceAdmin
 *
 * @package AppBundle\Entity\Admin
 */
class DeviceAdmin extends Admin
{
    public $last_position = 50;

    /** @var string */
    protected $baseRoutePattern = 'device';

    /** @var string */
    protected $baseRouteName = 'device';

    protected $datagridValues = [
        '_page'       => 1,
        '_sort_order' => 'DESC',
        '_sort_by'    => 'created',
    ];

    /**
     * @param ShowMapper $showMapper
     */
    public function configureShowFields(ShowMapper $showMapper)
    {
        parent::configureShowFields($showMapper);
        $showMapper
            ->with('Name', ['class' => 'col-sm-4 col-md-4'])
            ->add('id')
            ->add('name', null, ['label' => 'Device'])
            ->add('owner', null, ['label' => 'Owner'])
            ->end()
            ->with('Address', ['class' => 'col-sm-4 col-md-4'])
            ->add('address')
            ->end()
            ->with('Bandwidth & Device type', ['class' => 'col-sm-4 col-md-4'])
            ->add('bandwidth', ArrayCollection::class)
            ->add('ipAddress', ArrayCollection::class)
            ->add('deviceType', ArrayCollection::class)
            ->end();
        $showMapper->with('Statistics', ['class' => 'col-sm-4 col-md-4'])
            ->add('status', 'boolean', ['label' => 'Active?',])
            ->add('created', 'datetime', ['label' => 'Creation date'])
            ->add('modified', 'datetime', ['label' => 'Last update date'])
            ->end();
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        parent::configureFormFields($formMapper);
        $formMapper
            ->with('Name', ['class' => 'col-sm-4 col-md-4'])
            ->add('name', null, ['label' => 'Device'])
            ->add('owner', null, ['label' => 'Owner'])
            ->add('address')
            ->add('status', ChoiceType::class, ['choices' => [1 => 'Active', 0 => 'Inactive']])
            ->end()
            ->with('Bandwidth & Device type', ['class' => 'col-sm-4 col-md-4'])
            ->add('ipAddresses')
            ->add('bandwidth')
            ->add('deviceType', null, ['label' => 'Type'])
            ->end();
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        parent::configureDatagridFilters($datagridMapper);
        $datagridMapper
            ->add('name', null, ['label' => 'Device']);
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        parent::configureListFields($listMapper);
        $listMapper
            ->addIdentifier('id')
            ->add('name', null, ['label' => 'Device'])
            ->add('owner', null, ['label' => 'Owner'])
            ->add('address')
            ->add('ipAddresses', ArrayCollection::class)
            ->add('bandwidth', ArrayCollection::class)
            ->add('deviceType', ArrayCollection::class, ['label' => 'Type'])
            ->add('status', 'boolean', ['label' => 'Active?', 'editable' => true])
            ->add(
                '_action',
                'actions',
                [
                    'actions' => [
                        'show'   => [],
                        'edit'   => [],
                        'delete' => [],
                    ]
                ]
            );
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        // to remove a single route
//        $collection->remove('create');
    }

    public function prePersist($device)
    {
        $this->preUpdate($device);
    }

    public function preUpdate($device)
    {
        $device->setIpAddresses($device->getIpAddresses());
        if ($device->getStatus() == Device::STATUS_INACTIVE) {
            /** @var IpAddress $ipAddress */
            foreach ($device->getIpAddresses() as $ipAddress) {
                $ipAddress->setStatus(IpAddress::STATUS_INACTIVE);
            }
        }
    }
}
