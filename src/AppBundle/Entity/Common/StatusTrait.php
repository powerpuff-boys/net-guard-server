<?php

namespace AppBundle\Entity\Common;

use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Status
 * @package MktpSellers\DefaultBundle\Entity\Common
 */
trait StatusTrait
{
    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status = 1;

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @param int $status
     *
     * @return string
     *
     * @codeCoverageIgnore
     */
    public static function getTextForStatus(int $status): string
    {
        $mapping = static::getAvailableStatusesAsText();
        if (!array_key_exists($status, $mapping)) {
            throw new \InvalidArgumentException("The status \"{$status}\" does not exist.");
        }

        return $mapping[$status];
    }

    /**
     * @return array
     *
     * @codeCoverageIgnore
     */
    public static function getAvailableStatusesAsText(): array
    {
        return [];
    }
}
