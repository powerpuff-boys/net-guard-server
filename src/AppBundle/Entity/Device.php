<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Common\StatusTrait;
use AppBundle\Entity\Common\TimestampableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Device
 *
 * @ORM\Entity
 *
 * @ORM\Table(name="devices", indexes={
 *     @ORM\Index(name="status", columns={"status"})
 * })
 *
 * @codeCoverageIgnore
 *
 * @package AppBundle\Entity
 */
class Device
{
    /** @const int */
    const STATUS_ACTIVE = 1;

    /** @const int */
    const STATUS_INACTIVE = 0;

    use TimestampableTrait;
    use StatusTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="owner", type="string", length=255, nullable=true)
     */
    private $owner;

    /**
     * @var IpAddress
     *
     * @ORM\OneToMany(targetEntity="IpAddress", mappedBy="device", cascade={"persist"}, orphanRemoval=true)
     **/
    private $ipAddresses;

    /**
     * @var Bandwidth
     *
     * @ORM\ManyToOne(targetEntity="Bandwidth")
     * @ORM\JoinColumn(name="bandwidth_id", referencedColumnName="id")
     **/
    private $bandwidth;

    /**
     * @var DeviceType
     *
     * @ORM\ManyToOne(targetEntity="DeviceType")
     * @ORM\JoinColumn(name="device_type_id", referencedColumnName="id")
     **/
    private $deviceType;

    public function __toString()
    {
        return (string)$this->getName() ?: 'New';
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Device
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Device
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set bandwidth
     *
     * @param Bandwidth $bandwidth
     *
     * @return Device
     */
    public function setBandwidth(Bandwidth $bandwidth = null)
    {
        $this->bandwidth = $bandwidth;

        return $this;
    }

    /**
     * Get bandwidth
     *
     * @return Bandwidth
     */
    public function getBandwidth()
    {
        return $this->bandwidth;
    }

    /**
     * Set deviceType
     *
     * @param DeviceType $deviceType
     *
     * @return Device
     */
    public function setDeviceType(DeviceType $deviceType = null)
    {
        $this->deviceType = $deviceType;

        return $this;
    }

    /**
     * Get deviceType
     *
     * @return DeviceType
     */
    public function getDeviceType()
    {
        return $this->deviceType;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ipAddresses = new ArrayCollection();
    }

    /**
     * Add ipAddress
     *
     * @param IpAddress $ipAddress
     *
     * @return Device
     */
    public function addIpAddress(IpAddress $ipAddress)
    {
        $ipAddress->setDevice($this);

        $this->ipAddresses[] = $ipAddress;

        return $this;
    }

    /**
     * Remove ipAddress
     *
     * @param IpAddress $ipAddress
     */
    public function removeIpAddress(IpAddress $ipAddress)
    {
        $this->ipAddresses->removeElement($ipAddress);
    }

    /**
     * Get ipAddresses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIpAddresses()
    {
        return $this->ipAddresses;
    }

    public function setIpAddresses($ipAddresses)
    {
        if (count($ipAddresses) > 0) {
            foreach ($ipAddresses as $i) {
                $this->addIpAddress($i);
            }
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param string $owner
     *
     * @return $this
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;

        return $this;
    }
}
