<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Common\StatusTrait;
use AppBundle\Entity\Common\TimestampableTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class IpAddress
 *
 * @ORM\Entity
 *
 * @ORM\Table(name="ip_addresses", indexes={
 *     @ORM\Index(name="status", columns={"status"})
 * })
 *
 * @codeCoverageIgnore
 *
 * @package AppBundle\Entity
 */
class IpAddress
{
    /** @const int */
    const STATUS_ACTIVE = 1;

    /** @const int */
    const STATUS_INACTIVE = 0;

    use TimestampableTrait;
    use StatusTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ipv4", type="string", nullable=true)
     */
    private $ipv4;

    /**
     * @var string
     *
     * @ORM\Column(name="mac", type="string", length=50, nullable=false)
     */
    private $mac;

    /**
     * @var Device
     *
     * @ORM\ManyToOne(targetEntity="Device", inversedBy="ipAddresses")
     * @ORM\JoinColumn(name="device_id", referencedColumnName="id")
     **/
    private $device;

    /**
     * @var PointOfPresence
     *
     * @ORM\ManyToOne(targetEntity="PointOfPresence")
     * @ORM\JoinColumn(name="pop_id", referencedColumnName="id")
     **/
    private $pointOfPresence;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ipv4
     *
     * @param integer $ipv4
     *
     * @return IpAddress
     */
    public function setIpv4($ipv4)
    {
        $this->ipv4 = ip2long($ipv4);

        return $this;
    }

    /**
     * Get ipv4
     *
     * @return integer
     */
    public function getIpv4()
    {
        return long2ip($this->ipv4);
    }

    /**
     * Set mac
     *
     * @param string $mac
     *
     * @return IpAddress
     */
    public function setMac($mac)
    {
        $this->mac = $mac;

        return $this;
    }

    /**
     * Get mac
     *
     * @return string
     */
    public function getMac()
    {
        return $this->mac;
    }

    /**
     * Set pointOfPresence
     *
     * @param PointOfPresence $pointOfPresence
     *
     * @return IpAddress
     */
    public function setPointOfPresence(PointOfPresence $pointOfPresence = null)
    {
        $this->pointOfPresence = $pointOfPresence;

        return $this;
    }

    /**
     * Get pointOfPresence
     *
     * @return PointOfPresence
     */
    public function getPointOfPresence()
    {
        return $this->pointOfPresence;
    }


    public function __toString()
    {
        return (string)$this->getIpv4() . ' (' . $this->getPointOfPresence() . ')' ?: 'New';
    }

    /**
     * Set device
     *
     * @param Device $device
     *
     * @return IpAddress
     */
    public function setDevice(Device $device = null)
    {
        $this->device = $device;

        return $this;
    }

    /**
     * Get device
     *
     * @return Device
     */
    public function getDevice()
    {
        return $this->device;
    }
}
