<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Common\StatusTrait;
use AppBundle\Entity\Common\TimestampableTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class DeviceType
 *
 * @ORM\Entity
 *
 * @ORM\Table(name="device_types", indexes={ @ORM\Index(name="status", columns={"status"}) })
 *
 * @codeCoverageIgnore
 *
 * @package AppBundle\Entity
 */
class DeviceType
{
    /** @const int */
    const STATUS_ACTIVE = 1;

    /** @const int */
    const STATUS_INACTIVE = 0;

    use TimestampableTrait;
    use StatusTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=false)
     */
    private $name;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function __toString()
    {
        return (string)$this->getName() ?: 'New';
    }
}
