<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Common\StatusTrait;
use AppBundle\Entity\Common\TimestampableTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Bandwidth
 *
 * @ORM\Entity
 *
 * @ORM\Table(name="bandwidths", indexes={ @ORM\Index(name="status", columns={"status"}) })
 *
 * @codeCoverageIgnore
 *
 * @package AppBundle\Entity
 */
class Bandwidth
{
    /** @const int */
    const STATUS_ACTIVE = 1;

    /** @const int */
    const STATUS_INACTIVE = 0;

    use TimestampableTrait;
    use StatusTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="value", type="integer", nullable=true)
     */
    private $value;

    /**
     * Bandwidth constructor.
     */
    public function __toString()
    {
        return (string)$this->getValue() . " Kbps" ?: 'New';
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param int $value
     *
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }
}
